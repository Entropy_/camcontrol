import QtQuick 2.4
import QtQuick.Layouts 1.1
import Ubuntu.Components 1.3
import QtMultimedia 5.0

MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'camcontrol.localtoast'
    automaticOrientation: true

    width: units.gu(45)
    height: units.gu(75)

    Page {
        anchors.fill: parent
    Camera {
        id: camera
        imageCapture {
            onImageCaptured: {
                // Show the preview in an Image
                photoPreview.source = preview

            }
        }
    }

        header: PageHeader {
            id: header
            title: i18n.tr('camcontrol')
        }

    VideoOutput {
        source: camera
        focus : visible // to receive focus and capture key events when visible
        anchors.fill: parent
        MouseArea {
            anchors.fill: parent;
            onClicked: {
                header.title = i18n.tr('boop!')
            }
        }
    }
    Image {
        id: photoPreview
            
    }
    }
}